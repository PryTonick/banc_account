public class Card {
    private String number_card = "2200 2108 7373 7515";
    private String cardhoulder_name = "Nikita Gorshkov";
    private int money;
    private int count;

    Card(int money){
        this.money = money;
    }

    public void auto_refill(){
        for(int i = 1 ; i <= 100_000 ; i++){
            money += 10 + (Math.random() * 10);
            if( money >= 1000){
                count++;
                withdrawal_of_money();
            }
        }
    }

    private void withdrawal_of_money(){
        money -= 1000;
    }

    public String toString(){
        return "Номер карты : " + number_card + " | " + "хозяин карты : " + cardhoulder_name + " | "
                + "Балланс карты = " + money + " рублей" + " | " + "совершено операций по выводу средств: " + count;
    }
}